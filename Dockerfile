FROM golang:1.14.1-alpine3.11 as BUILDER

ARG PORT=3000

WORKDIR /chatservice

ADD . .

RUN go build -o chatservice ./cmd/chatservice.go

FROM alpine:3.11

COPY --from=BUILDER /chatservice/chatservice /bin/.

EXPOSE $PORT

CMD ["chatservice"]
