package db

import (
	"context"
)

type DB interface {
	Configure() (DB, error)
	DbExists(name string) (bool, error)
	CreateDb(name, schema string) error
	Insert(index string, ctx context.Context, body interface{}) (interface{}, error)
	Get(index string, ctx context.Context, body interface{}) (interface{}, error)
}
