package es

import (
	"context"
	"errors"
	// "encoding/json"
	// "fmt"

	"gitlab.com/kennylouie/chatservice/pkg/db"

	"github.com/olivere/elastic/v7"
)

const (
	DocType = "_doc"
)

type es struct {
	host   string
	client *elastic.Client
}

func New(host string) *es {
	return &es{
		host:   host,
		client: nil,
	}
}

func (e *es) Configure() (db.DB, error) {
	client, err := elastic.NewClient(
		elastic.SetURL(e.host),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
	)
	if err != nil {
		return e, err
	}

	e.client = client

	return e, nil
}

func (e es) CreateDb(name, schema string) error {
	ctx := context.Background()

	res, err := e.client.CreateIndex(name).BodyString(schema).Do(ctx)
	if err != nil {
		return err
	}

	if !res.Acknowledged {
		return errors.New("Registering was not acknowledged")
	}

	return nil
}

func (e es) DbExists(name string) (bool, error) {
	ctx := context.Background()

	exists, err := e.client.IndexExists(name).Do(ctx)
	if err != nil {
		return false, err
	}

	return exists, nil
}

func (e es) Insert(index string, ctx context.Context, body interface{}) (interface{}, error) {
	resp, err := e.client.Index().
		Type(DocType).
		Index(index).
		BodyJson(body).
		Do(ctx)
	if err != nil {
		return "", err
	}

	return resp, nil
}

const (
	Match = "match"
	Term  = "term"
)

type esQuery struct {
	fuzzy   bool
	minimum string
	matches map[string]string
	terms   map[string]string
}

func NewEsQuery(fuzzy bool, minimum string, matches, terms map[string]string) *esQuery {
	return &esQuery{
		fuzzy:   fuzzy,
		minimum: minimum,
		matches: matches,
		terms:   terms,
	}
}

func (e es) Get(index string, ctx context.Context, body interface{}) (interface{}, error) {
	b := body.(*esQuery)

	q := elastic.NewBoolQuery()

	for k, v := range b.matches {
		q = q.Must(elastic.NewMatchQuery(k, v))
	}

	if b.fuzzy {
		q.MinimumShouldMatch(b.minimum)
		for k, v := range body.(esQuery).matches {
			q = q.Should(elastic.NewMatchPhraseQuery(k, v).Slop(50))
		}
	} else {
		for k, v := range b.terms {
			q = q.Must(elastic.NewTermQuery(k, v))
		}
	}

	searchSource := elastic.NewSearchSource().Query(q)

	// str, _ := searchSource.Source()
	// js, _ := json.Marshal(str)
	// fmt.Println("[esclient]Final ESQuery=\n", string(js))

	hits, err := e.client.Search().Index(index).SearchSource(searchSource).Do(ctx)
	if err != nil {
		return nil, err
	}

	return hits, nil
}

type EsResult struct {
	*elastic.SearchResult
}

func ApplyAsEsResult(e interface{}) *EsResult {
	return &EsResult{
		e.(*elastic.SearchResult),
	}
}
