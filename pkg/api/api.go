package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type Api struct {
	logger *zap.Logger
	router *mux.Router
	port   string
}

func New(logger *zap.Logger, port string) *Api {
	return &Api{
		logger: logger,
		router: mux.NewRouter().StrictSlash(true),
		port:   port,
	}
}

func (a *Api) LoadRoute(r *Route) *Api {
	a.router.Methods(r.Method).
		Path(r.Path).
		Name(r.Name).
		Handler(r.Handler)

	a.logger.Info(fmt.Sprintf("Loaded route %s", r.Name))

	return a
}

func (a Api) Serve() {
	srv := &http.Server{
		Addr:         fmt.Sprintf(":%s", a.port),
		Handler:      a.router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 20 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	a.logger.Info(fmt.Sprintf("Server serving on port %s", a.port))

	if err := srv.ListenAndServe(); err != nil {
		a.logger.Fatal("Error in serving server", zap.Error(err))
	}
}
