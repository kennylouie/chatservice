package api

import (
	"net/http"
)

type Route struct {
	Name    string
	Path    string
	Handler http.HandlerFunc
	Method  string
}
