package health

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/kennylouie/chatservice/pkg/conf"

	"go.uber.org/zap"
)

func TestHealth(t *testing.T) {
	c := zap.NewProductionConfig()
	c.OutputPaths = []string{"stdout"}
	logger, err := c.Build()
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not init zap logger: %v", err))
	}
	defer logger.Sync()

	a := New(logger, conf.Conf{}, nil)
	a.Load_routes()

	req, _ := http.NewRequest(http.MethodGet, "/api/v1/health", nil)
	rr := httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusOK {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}
}
