package health

import (
	"encoding/json"
	"net/http"

	"gitlab.com/kennylouie/chatservice/pkg/api"

	"go.uber.org/zap"
)

type healthService struct {
	logger *zap.Logger
}

func New(logger *zap.Logger) *healthService {
	return &healthService{
		logger: logger,
	}
}

func (h healthService) GetHealthRoute() *api.Route {
	return &api.Route{
		Name:    "health",
		Path:    "/health",
		Handler: h.HandleHealth,
		Method:  http.MethodGet,
	}
}

func (h healthService) HandleHealth(w http.ResponseWriter, r *http.Request) {

	h.logger.Info("requested health endpoint")

	json.NewEncoder(w).Encode(map[string]string{"response": "live"})
}
