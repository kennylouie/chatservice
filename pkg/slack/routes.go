package slack

import (
	"net/http"

	"gitlab.com/kennylouie/chatservice/pkg/api"
)

func (s slackService) GetSlashRoute() *api.Route {
	return &api.Route{
		Name:    "slack_slash",
		Path:    "/api/v1/slack/slash",
		Handler: s.HandleSlash,
		Method:  http.MethodPost,
	}
}

func (s slackService) GetInteractionsRoute() *api.Route {
	return &api.Route{
		Name:    "slack_interactions",
		Path:    "/api/v1/slack/interactions",
		Handler: s.HandleInteractions,
		Method:  http.MethodPost,
	}
}
