package slack

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/kennylouie/chatservice/pkg/questions"

	"github.com/slack-go/slack"
	"go.uber.org/zap"
)

func (s slackService) HandleSlash(w http.ResponseWriter, r *http.Request) {
	s.logger.Info("requested slack slash endpoint")

	msg, err := s.ProcessSlash(r)
	if err != nil {
		s.logger.Error("Could not process slash command", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	b, err := json.MarshalIndent(msg, "", "    ")
	if err != nil {
		s.logger.Error("Could not marshal message payload", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(b)

	s.logger.Info("Slash message payload sucessfully sent")
}

func (s slackService) ProcessSlash(r *http.Request) (slack.Message, error) {
	slash, err := slack.SlashCommandParse(r)
	if err != nil {
		return slack.Message{}, err
	}

	switch slash.Command {
	case s.slashCommand:
		// save to elasticsearch
		ctx := context.Background()

		_, err := s.db.Insert(
			questions.IndexQuestions,
			ctx,
			questions.NewQuestion(slash.Text, slash.UserName),
		)
		if err != nil {
			return slack.Message{}, err
		}

		// respond with a message to slack
		m := NewQuestionMessage(slash.Text, slash.UserName)

		return m.Generate(), nil
	default:
		return slack.Message{}, errors.New("Unknown command")
	}
}

const (
	contextText    = "Asked by @%v"
	viewButtonText = "View"
	viewButtonId   = "viewButton"

	answerButtonText = "Answer"
	answerButtonId   = "answerButton"
)

type questionMessage struct {
	text     string
	username string
}

func NewQuestionMessage(text, username string) *questionMessage {
	return &questionMessage{
		text:     text,
		username: username,
	}
}

func (q questionMessage) Generate() slack.Message {
	// Fields
	questionField := slack.NewTextBlockObject("mrkdwn", q.text, false, false)
	fieldSlice := make([]*slack.TextBlockObject, 0)
	fieldSlice = append(fieldSlice, questionField)

	fieldsSection := slack.NewSectionBlock(nil, fieldSlice, nil)

	// Context
	userContextContent := fmt.Sprintf(contextText, q.username)
	userContextText := slack.NewTextBlockObject(slack.MarkdownType, userContextContent, false, false)
	userContextBlock := slack.NewContextBlock("context", userContextText)

	// Action Buttons
	viewBtnTxt := slack.NewTextBlockObject("plain_text", viewButtonText, false, false)
	viewBtn := slack.NewButtonBlockElement("", viewButtonId, viewBtnTxt)
	answerBtnTxt := slack.NewTextBlockObject("plain_text", answerButtonText, false, false)
	answerBtn := slack.NewButtonBlockElement("", answerButtonId, answerBtnTxt).WithStyle("primary")
	actionBlock := slack.NewActionBlock("", viewBtn, answerBtn)

	// Build Message with blocks created above
	msg := slack.NewBlockMessage(
		fieldsSection,
		userContextBlock,
		actionBlock,
	)

	msg.ResponseType = slack.ResponseTypeInChannel

	return msg
}
