package slack

import (
	"fmt"

	"gitlab.com/kennylouie/chatservice/pkg/db"
	"gitlab.com/kennylouie/chatservice/pkg/questions"

	"github.com/slack-go/slack"
	"go.uber.org/zap"
)

type slackService struct {
	token         string
	signingSecret string
	slashCommand  string
	api           *slack.Client
	db            db.DB
	logger        *zap.Logger
}

func New(token, signingSecret, slashCommand string, db db.DB, logger *zap.Logger) *slackService {
	return &slackService{
		token:         token,
		signingSecret: signingSecret,
		slashCommand:  slashCommand,
		api:           slack.New(token),
		db:            db,
		logger:        logger,
	}
}

func (s *slackService) Configure() *slackService {
	// create questions index if not exists
	exists, err := s.db.DbExists(questions.IndexQuestions)
	if err != nil {
		s.logger.Fatal("Could not check es index", zap.Error(err))
	}

	if !exists {
		s.logger.Info(fmt.Sprintf("%s index does not exist, attempting to create index", questions.IndexQuestions))

		err := s.db.CreateDb(questions.IndexQuestions, questions.EsQuestionMapping)
		if err != nil {
			s.logger.Fatal("Could create es index", zap.Error(err))
		}
	}

	s.logger.Info(fmt.Sprintf("%s index exists", questions.IndexQuestions))

	// create answers index if not exists
	exists, err = s.db.DbExists(questions.IndexAnswers)
	if err != nil {
		s.logger.Fatal("Could not check es index", zap.Error(err))
	}

	if !exists {
		s.logger.Info(fmt.Sprintf("%s index does not exist, attempting to create index", questions.IndexAnswers))

		err := s.db.CreateDb(questions.IndexAnswers, questions.EsAnswerMapping)
		if err != nil {
			s.logger.Fatal("Could create es index", zap.Error(err))
		}
	}

	s.logger.Info(fmt.Sprintf("%s index exists", questions.IndexAnswers))

	return s
}
