package slack

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/kennylouie/chatservice/pkg/db/es"
	"gitlab.com/kennylouie/chatservice/pkg/questions"

	"github.com/slack-go/slack"
	"go.uber.org/zap"
)

func (s slackService) HandleInteractions(w http.ResponseWriter, r *http.Request) {
	s.logger.Info("requested slack interactions endpoint")

	err := s.ProcessInteractions(r)
	if err != nil {
		s.logger.Error("Could not process interactions payload", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.logger.Info("Interaction successfully processed")
}

func (s slackService) ProcessInteractions(r *http.Request) error {
	var payload slack.InteractionCallback

	err := json.Unmarshal([]byte(r.FormValue("payload")), &payload)
	if err != nil {
		return err
	}

	switch payload.Type {
	case slack.InteractionTypeBlockActions:
		s.logger.Info("Processing block action: button")
		var message string

	blockLoop:
		for _, b := range payload.Message.Blocks.BlockSet {
			switch b.BlockType() {
			case slack.MBTSection:
				s := b.(*slack.SectionBlock)
				message = s.Fields[0].Text
				break blockLoop
			default:
				return errors.New(fmt.Sprintf("Unexpected block type %s found from answer button's message", b.BlockType()))
			}
		}

		p := newButtonActionPayload(
			message,
			payload.User.Name,
			payload.User.ID,
			payload.ActionCallback.BlockActions[0].Value,
			payload.TriggerID,
		)

		var buttonId string

	actionLoop:
		for _, b := range payload.ActionCallback.BlockActions {
			switch string(b.Type) {
			case string(slack.METButton):
				buttonId = b.Value
				break actionLoop
			default:
				return errors.New(fmt.Sprintf("Unknown block type found in block action: %v", b.Type))
			}
		}

		switch buttonId {
		case viewButtonId:
			s.logger.Info("Processing button: view")

			// query answers in elasticsearch
			query := es.NewEsQuery(
				false, "",
				map[string]string{
					"question": message,
				},
				map[string]string{},
			)

			ctx := context.Background()

			hits, err := s.db.Get(questions.IndexAnswers, ctx, query)
			if err != nil {
				return err
			}

			h := es.ApplyAsEsResult(hits)

			answers := []questions.Answer{}

			for _, v := range h.Hits.Hits {
				var a questions.Answer

				err = json.Unmarshal(v.Source, &a)
				if err != nil {
					return err
				}

				if message != a.Question {
					return errors.New("Did not find matching question")
				}

				answers = append(answers, a)
			}

			req := p.newViewRequest(answers)
			_, err = s.api.OpenView(payload.TriggerID, req)
			if err != nil {
				return fmt.Errorf("Could not send modal view response: %v", err)
			}
		case answerButtonId:
			s.logger.Info("Processing button: answer")
			req := p.newAnswerRequest()
			_, err = s.api.OpenView(payload.TriggerID, req)
			if err != nil {
				return err
			}
		default:
			return errors.New("Unknown block action")
		}
	case slack.InteractionTypeViewSubmission:
		s.logger.Info("Processing block action: answer modal")

		var message string

	modalBlockLoop:
		for _, b := range payload.View.Blocks.BlockSet {
			switch b.BlockType() {
			case slack.MBTInput:
				s := b.(*slack.InputBlock)
				message = s.Label.Text
				break modalBlockLoop
			default:
				return errors.New(fmt.Sprintf("Unexpected block type %s found from answer button's message", b.BlockType()))
			}
		}

		// query question in elasticsearch
		query := es.NewEsQuery(
			false, "",
			map[string]string{
				"question": message,
			},
			map[string]string{
				"user": "kdev",
			},
		)

		ctx := context.Background()

		hits, err := s.db.Get(questions.IndexQuestions, ctx, query)
		if err != nil {
			return err
		}

		h := es.ApplyAsEsResult(hits)

		var q questions.Question

		// assuming only 1 question found
		err = json.Unmarshal(h.Hits.Hits[0].Source, &q)
		if err != nil {
			return err
		}

		if message != q.Question {
			return errors.New("Did not find matching question")
		}

		answer := payload.View.State.Values["Answer"]["answer"].Value
		user := payload.User.Name

		// save answer to elasticsearch
		_, err = s.db.Insert(
			questions.IndexAnswers,
			ctx,
			questions.NewAnswer(
				message,
				h.Hits.Hits[0].Id,
				answer,
				user,
			),
		)
		if err != nil {
			return err
		}
	default:
		return errors.New(fmt.Sprintf("Unknown interaction action: %s", payload.Type))
	}

	return nil
}

type buttonActionPayload struct {
	question      string
	username      string
	userID        string
	buttonClicked string
	triggerID     string
}

func newButtonActionPayload(question, username, userID, buttonClicked, triggerID string) *buttonActionPayload {
	return &buttonActionPayload{
		question:      question,
		username:      username,
		userID:        userID,
		buttonClicked: buttonClicked,
		triggerID:     triggerID,
	}
}

const (
	modalTitle        = "Add an answer"
	cancelButton      = "Cancel"
	submitButton      = "Submit"
	answerPlaceholder = "Write something"

	viewAnswers = "View Answers"
	closeButton = "Close"
)

func (p buttonActionPayload) newAnswerRequest() slack.ModalViewRequest {
	titleText := slack.NewTextBlockObject("plain_text", modalTitle, false, false)
	closeText := slack.NewTextBlockObject("plain_text", cancelButton, false, false)
	submitText := slack.NewTextBlockObject("plain_text", submitButton, false, false)

	answerText := slack.NewTextBlockObject("plain_text", p.question, false, false)
	answerPlaceholder := slack.NewTextBlockObject("plain_text", answerPlaceholder, false, false)
	answerElement := slack.NewPlainTextInputBlockElement(answerPlaceholder, "answer")
	answerElement.Multiline = true
	answer := slack.NewInputBlock("Answer", answerText, answerElement)

	blocks := slack.Blocks{
		BlockSet: []slack.Block{
			answer,
		},
	}

	var modalRequest slack.ModalViewRequest
	modalRequest.Type = slack.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Submit = submitText
	modalRequest.Blocks = blocks

	return modalRequest
}

func (p buttonActionPayload) newViewRequest(answers []questions.Answer) slack.ModalViewRequest {
	// header
	headerText := slack.NewTextBlockObject("mrkdwn", p.question, false, false)
	headerSection := slack.NewSectionBlock(headerText, nil, nil)

	blocks := slack.Blocks{
		BlockSet: []slack.Block{
			headerSection,
			slack.NewDividerBlock(),
		},
	}

	for _, a := range answers {
		answerText := slack.NewTextBlockObject("mrkdwn", a.Answer, false, false)
		userText := slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf("Answered by @%v %v", a.User, a.Timestamp), false, false)

		b := slack.NewSectionBlock(answerText, nil, nil)
		u := slack.NewSectionBlock(userText, nil, nil)

		blocks.BlockSet = append(blocks.BlockSet, b, u, slack.NewDividerBlock())
	}

	titleText := slack.NewTextBlockObject("plain_text", viewAnswers, false, false)
	closeText := slack.NewTextBlockObject("plain_text", closeButton, false, false)
	var modalRequest slack.ModalViewRequest

	modalRequest.Type = slack.ViewType("modal")
	modalRequest.Title = titleText
	modalRequest.Close = closeText
	modalRequest.Blocks = blocks

	return modalRequest
}
