package slack

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/kennylouie/chatservice/pkg/conf"

	"go.uber.org/zap"
)

func TestHealthHandler(t *testing.T) {
	c := zap.NewProductionConfig()
	c.OutputPaths = []string{"stdout"}
	logger, err := c.Build()
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not init zap logger: %v", err))
	}
	defer logger.Sync()

	a := New(logger, conf.Conf{}, nil)
	a.Load_routes()

	// test actual payload from slack slash
	req, _ := http.NewRequest(http.MethodPost, "/api/v1/slack/slash", nil)
	rr := httptest.NewRecorder()
	a.router.ServeHTTP(rr, req)
	if code := rr.Code; code != http.StatusInternalServerError {
		t.Errorf("expected status %d but got %d\n", http.StatusOK, code)
	}
}
