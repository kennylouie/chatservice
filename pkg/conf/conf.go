package conf

import (
	"github.com/kelseyhightower/envconfig"
	"go.uber.org/zap"
)

type conf struct {
	// api
	API_PORT string `envconfig:"API_PORT" default:"3000"`

	// slack
	SLACK_TOKEN          string `envconfig:"SLACK_TOKEN"`
	SLACK_SIGNING_SECRET string `envconfig:"SLACK_SIGNING_SECRET"`
	SLACK_SLASH_COMMAND  string `envconfig:"SLACK_SLASH_COMMAND"`

	// es
	ES_HOST string `envconfig:"ES_HOST"`
}

func New(logger *zap.Logger) conf {
	c := conf{}

	err := envconfig.Process("", &c)
	if err != nil {
		logger.Fatal("Envconfig could not get env", zap.Error(err))
	}

	logger.Info("Envconfig success")

	return c
}

func (c conf) GetApiPort() string {
	return c.API_PORT
}

func (c conf) GetEsHost() string {
	return c.ES_HOST
}

func (c conf) GetSlackToken() string {
	return c.SLACK_TOKEN
}

func (c conf) GetSlackSigningSecret() string {
	return c.SLACK_SIGNING_SECRET
}

func (c conf) GetSlackSlashCommand() string {
	return c.SLACK_SLASH_COMMAND
}
