package main

import (
	"fmt"
	"log"

	"gitlab.com/kennylouie/chatservice/pkg/api"
	"gitlab.com/kennylouie/chatservice/pkg/conf"
	"gitlab.com/kennylouie/chatservice/pkg/db/es"
	"gitlab.com/kennylouie/chatservice/pkg/health"
	"gitlab.com/kennylouie/chatservice/pkg/slack"

	"go.uber.org/zap"
)

func main() {
	z := zap.NewProductionConfig()
	z.OutputPaths = []string{"stdout"}
	logger, err := z.Build()
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not init zap logger: %v", err))
	}
	defer logger.Sync()

	// env
	c := conf.New(logger.Named("conf_logger"))

	// db
	elastic, err := es.New(c.GetEsHost()).Configure()
	if err != nil {
		logger.Fatal("Could not configure es", zap.Error(err))
	}

	// api
	api := api.New(logger.Named("api_logger"), c.GetApiPort())

	// load health service
	h := health.New(logger.Named("health_logger"))

	// load slack service
	s := slack.New(
		c.GetSlackToken(),
		c.GetSlackSigningSecret(),
		c.GetSlackSlashCommand(),
		elastic,
		logger.Named("slack_logger"),
	).
		Configure()

	// load api routes and serve
	api.
		LoadRoute(h.GetHealthRoute()).
		LoadRoute(s.GetSlashRoute()).
		LoadRoute(s.GetInteractionsRoute()).
		Serve()
}
