module gitlab.com/kennylouie/chatservice

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mailru/easyjson v0.7.3 // indirect
	github.com/olivere/elastic v6.2.34+incompatible
	github.com/olivere/elastic/v7 v7.0.19
	github.com/slack-go/slack v0.6.6
	go.uber.org/zap v1.15.0
)
