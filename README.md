# Chat Events Interface Service

## Background

This service is used for interfacing different chat services, e.g. Slack, Discord, Keybase, and polling events from those services to be routed to a message broker, e.g. Kafka or RabbitMq.

## Development

### Tests

```
go test ./...
```

## Deploy

### Locally

The following environment variables need to be set in an `.env` file:

```
API_PORT=3000
SLACK_TOKEN=<your_token>
SLACK_VERIFICATION_TOKEN=<your_verification_token>
SLACK_SLASH_COMMAND=<your_slack_slash_command>
ES_HOST=http://127.0.0.1:9200
```

```
docker-compose up -d
```

This will build the image locally if it does not exist.

Note that if you've updated the code the run command will not auto build the image. To build:

```
docker-compose build api
```
